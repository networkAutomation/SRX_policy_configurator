#### DEPENDENCIES

`sudo pip3 install netmiko pyyaml netadrr`

#### Source/Destination IP, protocol and TCP/UDP ports YAML file example, saved as policyConfig.yaml

```
---
- SNOW:
    change: CHG0103393

- Sources:
  - ipSource:
      address:
        - 10.42.9.62
        - 10.42.9.63
      mask: 255.255.255.255

  - ipSource:
      address:
        - 10.40.13.0
      mask: 255.255.255.128

  - ipSource:
      address:
        - 10.42.9.0
        - 10.43.9.0
      mask: 255.255.255.224

  - ipSource:
      address:
        - 10.40.13.0
      mask: 255.255.255.192

  - ipSource:
      address:
        - 10.40.9.0
      mask: 255.255.255.0

- Destinations:
  - ipDestination:
      address:
        - 4.4.4.4
        - 4.4.4.7
      mask: 255.255.255.255

  - ipDestination:
      address:
        - 8.8.8.0
      mask: 255.255.255.0

- protPortDestination:
    protocol: tcp
    ports:
      - 80
      - 443
      - 8080
      - 7777

- protPortDestination:
    protocol: udp
    ports:
      - 53
      - 514
      - 1024

- zones:
    source: TRUST
    destination: UNTRUST
```

### firewall list YAML file example

```
---
  8.8.0.0/16: srx00.sw11.lab
  4.0.0.0/8: srx00.sw11.lab
```

### USAGE

Make sure you have your `firewallList.yaml` in the same directory you run the script

`python3 policyConfigurator.py policyConfig.yaml`

```
Please, provide login credentials:
Username: root
Password:
set security address-book UNTRUST address H-4.4.4.4-32 4.4.4.4/32
set security address-book UNTRUST address H-4.4.4.7-32 4.4.4.7/32
set security address-book TRUST address H-10.42.9.62-32 10.42.9.62/32
set security address-book TRUST address H-10.42.9.63-32 10.42.9.63/32
set security address-book TRUST address H-10.40.13.59-32 10.40.13.59/32
set security address-book TRUST address N-10.42.9.0-27 10.42.9.0/27
set security address-book TRUST address N-10.43.9.0-27 10.43.9.0/27
set security address-book TRUST address N-10.40.13.0-26 10.40.13.0/26
set security address-book TRUST address N-10.40.9.0-24 10.40.9.0/24
set security address-book UNTRUST address-set DG-CHG0103393 address H-4.4.4.4-32
set security address-book UNTRUST address-set DG-CHG0103393 address H-4.4.4.7-32
set security address-book TRUST address-set SG-CHG0103393 address H-10.42.9.62-32
set security address-book TRUST address-set SG-CHG0103393 address H-10.42.9.63-32
set security address-book TRUST address-set SG-CHG0103393 address H-10.40.13.59-32
set security address-book TRUST address-set SG-CHG0103393 address N-10.42.9.0-27
set security address-book TRUST address-set SG-CHG0103393 address N-10.43.9.0-27
set security address-book TRUST address-set SG-CHG0103393 address N-10.40.13.0-26
set security address-book TRUST address-set SG-CHG0103393 address N-10.40.9.0-24
set security policies from-zone TRUST to-zone UNTRUST policy CHG0103393 match source-address SG-CHG0103393
set security policies from-zone TRUST to-zone UNTRUST policy CHG0103393 match destination-address DG-CHG0103393
set security policies from-zone TRUST to-zone UNTRUST policy CHG0103393 match application junos-http
set security policies from-zone TRUST to-zone UNTRUST policy CHG0103393 match application junos-https
set applications application TCP-8080 protocol tcp destination-port 8080
set security policies from-zone TRUST to-zone UNTRUST policy CHG0103393 match application TCP-8080
set applications application TCP-7777 protocol tcp destination-port 7777
set security policies from-zone TRUST to-zone UNTRUST policy CHG0103393 match application TCP-7777
set security policies from-zone TRUST to-zone UNTRUST policy CHG0103393 match application junos-dns-udp
set security policies from-zone TRUST to-zone UNTRUST policy CHG0103393 match application junos-syslog
set applications application UDP-1024 protocol udp destination-port 1024
set security policies from-zone TRUST to-zone UNTRUST policy CHG0103393 match application UDP-1024
set security policies from-zone TRUST to-zone UNTRUST policy CHG0103393 then permit

Do you want proceed on srx00.sw11.lab? (yes/no): yes

########## CONFIGURATION IN PROGRESS ##########


########## CONFIGURATION CHECK - BEGIN ##########

show security address-book TRUST address-set SG-CHG0103393

address H-10.42.9.62-32;
address H-10.42.9.63-32;
address H-10.40.13.59-32;
address N-10.42.9.0-27;
address N-10.43.9.0-27;
address N-10.40.13.0-26;
address N-10.40.9.0-24;

show security address-book UNTRUST address-set DG-CHG0103393

address H-4.4.4.4-32;
address H-4.4.4.7-32;

show security policies from-zone TRUST to-zone UNTRUST policy CHG0103393

match {
    source-address SG-CHG0103393;
    destination-address DG-CHG0103393;
    application [ junos-http junos-https TCP-8080 TCP-7777 junos-dns-udp junos-syslog UDP-1024 ];
}
then {
    permit;
}

########## CONFIGURATION CHECK - END ##########

```
